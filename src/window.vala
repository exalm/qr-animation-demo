[GtkTemplate (ui = "/org/example/App/window.ui")]
public class QrAnimation.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Thing thing;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    [GtkCallback]
    private void play_cb () {
        thing.play ();
    }
}
