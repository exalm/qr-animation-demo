
namespace QrAnimation {
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: "org.example.App", flags: ApplicationFlags.FLAGS_NONE);
        }

        construct {
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                { "preferences", this.on_preferences_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) {
                win = new QrAnimation.Window (this);
            }
            win.present ();
        }

        private void on_about_action () {
            string[] authors = { "Alexander" };
            Gtk.show_about_dialog (this.active_window,
                                   "program-name", "qr-animation",
                                   "authors", authors,
                                   "version", "0.1.0");
        }

        private void on_preferences_action () {
            message ("app.preferences action activated");
        }
    }
}
