public class Thing : Gtk.Widget {
    class SquareInfo {
        public double x;
        public double y;
        public double size;
        public Adw.Animation animation;
    }

    private SquareInfo[] squares;
    private Adw.Animation next_anim;
    private int squares_remaining;

    private Adw.Animation icon_anim;
    private Gtk.SymbolicPaintable icon;

    construct {
        overflow = HIDDEN;
        squares = {};

        next_anim = new Adw.TimedAnimation (this, 0, 1, 20, new Adw.CallbackAnimationTarget (value => {}));
        next_anim.done.connect (() => {
            add_square (false);
            squares_remaining--;

            if (squares_remaining > 0)
                next_anim.play ();
            else {
                add_square (true);
                icon_anim.play ();
            }
        });

        var display = get_display ();
        var icon_theme = Gtk.IconTheme.get_for_display (display);
        icon = icon_theme.lookup_icon ("navigate-symbolic", null, 96, scale_factor, get_direction (), FORCE_SYMBOLIC);

        icon_anim = new Adw.SpringAnimation (this, 0, 96, new Adw.SpringParams (0.7, 1, 100), new Adw.CallbackAnimationTarget (queue_draw));
    }

    static construct {
        set_css_name ("thing");
    }

    public override void snapshot (Gtk.Snapshot snapshot) {
        foreach (var info in squares) {
            Gdk.RGBA rgba = { 1, 1, 1, 1 };

            float size = (float) info.size;
            float x = (float) info.x - size / 2;
            float y = (float) info.y - size / 2;
            Graphene.Rect bounds = {{ x, y }, { (float) info.size, (float) info.size }};

            snapshot.append_color (rgba, bounds);
        }

        int w = get_width ();
        int h = get_height ();
        float icon_size = (float) icon_anim.value;
        var rgba = get_style_context ().get_color ();
        snapshot.translate ({ (w - icon_size) / 2, (h - icon_size) / 2 });
        icon.snapshot_symbolic (snapshot, icon_size, icon_size, { rgba, rgba, rgba, rgba });
    }

    private void add_square (bool fill) {
        int w = get_width ();
        int h = get_height ();

        var square = new SquareInfo ();
        square.size = 0;
        squares += square;

        double size;

        if (fill) {
            square.x = w / 2;
            square.y = h / 2;
            size = int.max (w, h);
        } else {
            square.x = Random.double_range (0, w);
            square.y = Random.double_range (0, h);
            size = Random.double_range (50, 100);
        }
        
        square.animation = new Adw.TimedAnimation (this, 0, size, 250, new Adw.CallbackAnimationTarget (value => {
            square.size = value;
            queue_draw ();
        }));
        square.animation.play ();
    }

    public void play () {
        icon_anim.reset ();
        next_anim.reset ();

        squares = {};
        squares_remaining = 9;
        
        add_square (false);
        next_anim.play ();
    }
}
